<?php

function microcache_key($id, $fond) {
	include_spip('memoization_options');
	if (!function_exists('cache_set')) {
		return false;
	}
	return $fond . '-' . (is_numeric($id) ? $id : md5($id));
}

function supprimer_microcache($id, $fond) {
	if ($key = microcache_key($id, $fond)) {
		return cache_del($key);
	}
}

function microcache($id, $fond, $calcul = false) {
	$key = microcache_key($id, $fond);
	if (
		!$key
		|| $calcul
		|| in_array(_request('var_mode'), ['recalcul', 'debug'])
		|| !($contenu = cache_get($key))
	) {
		$contenu = recuperer_fond($fond, ['id' => $id]);
		if (
			$key
			&& _request('var_mode') != 'inclure'
			&& !$_POST
			&& !(isset($GLOBALS['var_nocache']) && $GLOBALS['var_nocache'])
			&& !defined('spip_interdire_cache')
		) {
			cache_set($key, $contenu, $ttl = 7 * 24 * 3600);
		}
	}
	return $contenu;
}
